resolvers += Resolver.url("sass-sbt-plugin-releases", url("https://dl.bintray.com/jcage/generic"))(Resolver.ivyStylePatterns)
resolvers += Resolver.url("heroku-sbt-plugin-releases", url("http://dl.bintray.com/heroku/sbt-plugins/"))(Resolver.ivyStylePatterns)

// The Play plugin
addSbtPlugin("com.typesafe.play" % "sbt-plugin" % "2.4.2")

// web plugins
addSbtPlugin("com.typesafe.sbt" % "sbt-digest" % "1.1.0")

// additional web plugins
addSbtPlugin("default" % "sbt-sass" % "0.1.9")
addSbtPlugin("com.heroku" % "sbt-heroku" % "0.4.3")