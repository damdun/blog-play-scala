package controllers

import models.{Article, Articles}
import play.api.data.Form
import play.api.data.Forms._
import play.api.mvc._
import slick.driver.SQLiteDriver.api._

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class Application extends Controller with ArticlesDatabase {

  val form = Form(
    mapping(
      "id" -> optional(longNumber),
      "title" -> nonEmptyText(minLength = 5),
      "text" -> nonEmptyText(minLength = 10)
    )(Article.apply)(Article.unapply)
  )

  def showList = Action.async {
    getArticles map { articles =>
      Ok(views.html.index(articles))
    }
  }

  def initCreate = Action {
    Ok(views.html.create(Article(None, "", "")))
  }

  def doCreate() = Action.async { implicit request =>
    form.bindFromRequest.fold(
      formWithErrors => {
        println(formWithErrors.errors)

        Future(
          BadRequest(views.html.create(Article(None, "", "")))
        )
      },
      article => {
        insertArticle(article) flatMap { _ =>
          getArticles map { articles =>
            Redirect(routes.Application.showList())
          }
        }
      }
    )
  }

  def showDetails(id: Long) = Action.async {
    findArticle(id) map { article =>
      Ok(views.html.show(article))
    }
  }

  def initEdit(id: Long) = Action.async {
    findArticle(id) map { article =>
      Ok(views.html.edit(article))
    }
  }

  def doEdit(id: Long) = Action.async { implicit request =>
    form.bindFromRequest.fold(
      formWithErrors => {
        println(formWithErrors.errors)

        findArticle(id) map { article =>
          BadRequest(views.html.edit(article))
        }
      },
      article => {
        updateArticle(id, article.copy(id = Some(id))) flatMap { _ =>
          getArticles map { articles =>
            Redirect(routes.Application.showList())
          }
        }
      }
    )
  }

  def doDelete(id: Long) = Action {
    Ok
  }

}

trait ArticlesDatabase {

  private val articles = TableQuery[Articles]

  private val db = Database.forConfig("database")

  db.run(DBIO.seq(
    // Create the tables, including primary and foreign keys
    articles.schema.create,
    // Insert some suppliers
    articles += Article(Some(1), "First look inside high speed train that will replace InterCity 125", "With a high speed service, sleek lines and modern technology, the InterCity 125 train was introduced to revamp the British rail network..."),
    articles += Article(Some(2), "Lord Mervyn Davies urges better rail and mobile services", "A former trade minister who is working to bring more financial services jobs to Wales said there is a risk firms could be put off unless rail and mobile services are improved...")
  ))

  protected def getArticles: Future[Seq[Article]] = {
    db.run(articles.result)
  }

  protected def insertArticle(article: Article) = {
    db.run(articles += article)
  }

  protected def findArticle(id: Long): Future[Article] = {
    db.run(findByID(id).result.head)
  }

  protected def updateArticle(id: Long, article: Article) = {
    db.run(findByID(id).update(article.copy(id = Some(id))))
  }

  private def findByID(id: Long) = {
    articles.filter(_.id === id)
  }

}