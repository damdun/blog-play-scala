package models

import slick.driver.SQLiteDriver.api._

case class Article(id: Option[Long], title: String, text: String)

class Articles(tag: Tag) extends Table[Article](tag, "ARTICLES") {

  def id = column[Long]("ID", O.PrimaryKey, O.AutoInc)

  def title = column[String]("TITLE")

  def text = column[String]("TEXT")

  def * = (id.?, title, text) <>(Article.tupled, Article.unapply)
}